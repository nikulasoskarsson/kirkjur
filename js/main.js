let currentStory = 0;

var renderStory = function(){
    document.querySelector("#story").innerHTML = createStory;
}
var getChapter = (n)=>{
    var chapters = [
        "Þegar fyrsta kirkjan var smíðuð þá var akkurat kreppan nýbyrjuð í Súgandafirði",
        "Eiríkur og Birkir hétu mennirnir sem smíðuðu hana og gerðu það báðir með annari hendi",
        "Þessi kirkja hafði allt að bera, t.a.m. hafís, súkkulaðikringlur og móttökunefnd",
        "And everyone lived happily ever after"
    ]
    return chapters[n];
}

var getTitle = (n)=>{
    var titles = [
        "Fyrsti kafli - Smíðin og kreppan",
        "Annar kafli - Með Annari",
        "Þriðji kafli - Hafísinn",
        "This is a title dog"
    ]
    return titles[n];
}




var wrapper =(n) =>{
    var html = 
    `
        <h1>${getTitle(n)}</h1>
        <p>${getChapter(n)}</p>
    `
    return html;
}


var renderChapter = (n)=>{
    if(getTitle(n)){
        document.querySelector("#story").innerHTML=wrapper(n);
    }
}

renderChapter(currentStory);
document.querySelector("#next").addEventListener("click",()=>{
    currentStory++;
    renderChapter(currentStory);
});

this===window?null:module.exports = {
    getChapter,
    getTitle,
    wrapper,
    renderChapter
};
