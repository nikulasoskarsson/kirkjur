# Kirkjur - kirkjusögur

This is an example project of how you can make tests for a simple website about simple things


## Getting Started

To start contributing to this project you should fork it to your own GitLab repository. Then you can clone it to your local repository on your machine.

### Prerequisites

You will need to have Node.js on your machine and this document will assume that you have yarn (npm should be ok too). To install yarn do:
npm install -g yarn
you will also need to have jest (that is the testing framework: https://jestjs.io/docs/en/getting-started). To install it just run the yarn command in the terminal in your project directory with no arguments like so:
 

```
yarn
```

### Opening the page in your browser

Once you fork this library to your own GitLab a CI process will start runing and after it has passed you should be able to run the page under settings > pages


## How to contribute to the project

If you want to add something to the project you will have to edit the main.js file. To add a chapter to the story you can add the text to the chapters array in the getChapters function like so:

```
var getChapter = (n)=>{
    var chapters = [
        "Þegar fyrsta kirkjan var smíðuð þá var akkurat kreppan nýbyrjuð í Súgandafirði",
        "Eiríkur og Birkir hétu mennirnir sem smíðuðu hana og gerðu það báðir með annari hendi",
        "Þessi kirkja hafði allt að bera, t.a.m. hafís, súkkulaðikringlur og móttökunefnd"
    ]
    return chapters[n];
}
```
Just remember to add a comma after the previous line. But if you are adding a new chapter you should also add title to that chapter like so:
```
var getTitle = (n)=>{
    var titles = [
        "Fyrsti kafli - Smíðin og kreppan",
        "Annar kafli - Með Annari",
        "Þriðji kafli - Hafísinn"
    ]
    return titles[n];
}
```
This is it. If you have added your chapter text and chapter name to the correct places you will have made a successful contribution to the project


### Running the tests

Before you commit your code and do a merge request you should test your code by typing

 **yarn test** 
into your terminal. If everything is successful you should see something like:

```
Test Suites: 1 passed, 1 total
Tests:       4 passed, 4 total
Snapshots:   0 total
Time:        1.089s
Ran all test suites.
Done in 1.76s.

```

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

